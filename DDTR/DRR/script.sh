#!/bin/bash

## Bash to compile and run
## the various configurations
## for the DDTR in DRR
if [ "$1" == "clean" ]
then
    rm -rf ./bin
elif [ "$1" == "build" ]
then
    # make directory bin if not exists
    mkdir -p ./bin

    # SLL_CL SLL_PK
    gcc drr.c -o ./bin/drrSLLSLL -pthread -lcdsl \
    -L./../synch_implementations \
    -I./../synch_implementations \
    -DSLL_CL -DSLL_PK 

    # SLL_CL DLL_PK
    gcc drr.c -o ./bin/drrSLLDLL -pthread -lcdsl \
    -L./../synch_implementations \
    -I./../synch_implementations \
    -DSLL_CL -DDLL_PK

    # SLL_CL DYN_ARR_PK
    gcc drr.c -o ./bin/drrSLLDYN -pthread -lcdsl \
    -L./../synch_implementations \
    -I./../synch_implementations \
    -DSLL_CL -DDYN_ARR_PK

    # DLL_CL SLL_PK
    gcc drr.c -o ./bin/drrDLLSLL -pthread -lcdsl \
    -L./../synch_implementations \
    -I./../synch_implementations \
    -DDLL_CL -DSLL_PK

    # DLL_CL DLL_PK
    gcc drr.c -o ./bin/drrDLLDLL -pthread -lcdsl \
    -L./../synch_implementations \
    -I./../synch_implementations \
    -DDLL_CL -DDLL_PK

    # DLL_CL DYN_ARR_PK
    gcc drr.c -o ./bin/drrDLLDYN -pthread -lcdsl \
    -L./../synch_implementations \
    -I./../synch_implementations \
    -DDLL_CL -DDYN_ARR_PK

    # DYN_ARR_CL SLL_PK
    gcc drr.c -o ./bin/drrDYNSLL -pthread -lcdsl \
    -L./../synch_implementations \
    -I./../synch_implementations \
    -DDYN_ARR_CL -DSLL_PK

    # DYN_ARR_CL DLL_PK
    gcc drr.c -o ./bin/drrDYNDLL -pthread -lcdsl \
    -L./../synch_implementations \
    -I./../synch_implementations \
    -DDYN_ARR_CL -DDLL_PK

    # DYN_ARR_CL DYN_ARR_PK
    gcc drr.c -o ./bin/drrDYNDYN -pthread -lcdsl \
    -L./../synch_implementations \
    -I./../synch_implementations \
    -DDYN_ARR_CL -DDYN_ARR_PK

elif [ "$1" == "run" ]
then
    mkdir -p ./logs
    if [ "$2" == "mem_access" ]
    then
        mkdir -p ./mem_access

        CONFIGURATIONS="SLLSLL SLLDLL SLLDYN"
        echo "Running mem access for $CONFIGURATION"
        for i in $CONFIGURATIONS
        do
            # add task on background
            valgrind --log-file="./logs/mem_access/log_$i.txt" \
            --tool=lackey --trace-mem=yes ./bin/drr$i &
        done
        wait
        CONFIGURATIONS="DLLSLL DLLDLL DLLDYN"
        echo "Running mem access for $CONFIGURATION"
        for i in $CONFIGURATIONS
        do
            # add task on background
            valgrind --log-file="./logs/mem_access/log_$i.txt" \
            --tool=lackey --trace-mem=yes ./bin/drr$i &
        done
        wait
        CONFIGURATIONS="DYNSLL DYNDLL DYNDYN"
        echo "Running mem access for $CONFIGURATION"
        for i in $CONFIGURATIONS
        do
            # add task on background
            valgrind --log-file="./logs/mem_access/log_$i.txt" \
            --tool=lackey --trace-mem=yes ./bin/drr$i &
        done
        wait
        echo "Finished"
    elif [ "$2" == "mem_footprint" ]
    then
        mkdir -p ./logs/mem_footprint
        CONFIGURATIONS="SLLSLL SLLDLL SLLDYN DLLSLL DLLDLL DLLDYN DYNSLL DYNDLL DYNDYN"
        echo "Running mem footprint"
        for i in $CONFIGURATIONS
        do
            valgrind --tool=massif ./bin/drr$i &&
            ms_print massif.out.* > ./logs/mem_footprint/log_$i.txt &&
            rm massif.out.*
        done
        echo "Finished"
    else
        echo "Choose between <mem_access, mem_footprint> example ./script.sh run mem_access"
    fi
elif [ "$1" == "count" ]
then
    if [ "$2" == "mem_access" ]
    then
        echo "mem_access"
        for i in $(ls logs/mem_access);
        do
            awk -v var="$i" '/I| L/{count++} END{print var " " count}' logs/mem_access/$i
        done
    elif [ "$2" == "mem_footprint" ]
    then
        echo "mem_footprint"
        for i in $(ls logs/mem_footprint);
        do
            #echo "$i"
            awk -v var="$i" '/KB|MB/{lol=$0;getline; print var " " $1 " " lol}' logs/mem_footprint/$i
        done
    else
        echo "Choose between <mem_access, mem_footprint> example ./script.sh count mem_footprint"
    fi
else
    echo "Need an argument <clean,build,run>"
fi
