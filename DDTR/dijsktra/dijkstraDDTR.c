#include <stdio.h>

#if defined(SLL)
#include "../synch_implementations/cdsl_queue.h"
#elif defined(DLL)
#include "../synch_implementations/cdsl_deque.h"
#elif defined(DYN)
#include "../synch_implementations/cdsl_dyn_array.h"
#endif

#define NUM_NODES                          100
#define NONE                               9999

struct _NODE
{
  int iDist;
  int iPrev;
};
typedef struct _NODE NODE;


struct _QITEM
{
  int iNode;
  int iDist;
  int iPrev;
  /* Dont use this struct but the DDTR queues */
#if defined(SLL) || defined(DLL) || defined(DYN)
#else
  struct _QITEM *qNext;
#endif
};
typedef struct _QITEM QITEM;

QITEM *qHead = NULL;


#if defined(SLL)
	cdsl_sll *ddt;
#elif defined(DLL)
	cdsl_dll *ddt;
#elif defined(DYN)
	cdsl_dyn_array *ddt;
#endif
       
int AdjMatrix[NUM_NODES][NUM_NODES];

int g_qCount = 0;
NODE rgnNodes[NUM_NODES];
int ch;
int iPrev, iNode;
int i, iCost, iDist;


void print_path (NODE *rgnNodes, int chNode)
{
  if (rgnNodes[chNode].iPrev != NONE)
    {
      print_path(rgnNodes, rgnNodes[chNode].iPrev);
    }
  printf (" %d", chNode);
  fflush(stdout);
}


void enqueue (int iNode, int iDist, int iPrev)
{
	QITEM *qNew = (QITEM *) malloc(sizeof(QITEM));
#if defined(SLL) || defined(DLL) || defined(DYN)
#else
	QITEM *qLast = qHead;
#endif
	
	if (!qNew) 
	{
		fprintf(stderr, "Out of memory.\n");
		exit(1);
	}
	qNew->iNode = iNode;
	qNew->iDist = iDist;
	qNew->iPrev = iPrev;

#if defined(SLL) || defined(DLL) || defined(DYN)
	ddt->enqueue(0, ddt, (void*) qNew);
#else
	qNew->qNext = NULL;

	if (!qLast) 
	{
		qHead = qNew;
	}
	else
	{
		while (qLast->qNext) qLast = qLast->qNext;
		qLast->qNext = qNew;
	}
#endif
	g_qCount++;

}


void dequeue (int *piNode, int *piDist, int *piPrev)
{
	QITEM *qKill;

#if defined(SLL) || defined(DLL) || defined(DYN)
	qKill = (QITEM*) ddt->get_head(0, ddt);
#else
  	qKill = qHead;
#endif


	if (qKill) // from qhead to qKill
		{
			*piNode = qKill->iNode; // qHead -> qKill 
			*piDist = qKill->iDist; // qHead -> qKill
			*piPrev = qKill->iPrev; // qHead -> qKill
#if defined(SLL) || defined(DLL) || defined(DYN)
			ddt->remove(0, ddt, qKill);
#else
			qHead = qHead->qNext;
#endif
			free(qKill);
			g_qCount--;
		}
}


int qcount (void)
{
  return(g_qCount);
}

int dijkstra(int chStart, int chEnd) 
{
  /* init of the distances */
  for (ch = 0; ch < NUM_NODES; ch++)
	{
		rgnNodes[ch].iDist = NONE;
		rgnNodes[ch].iPrev = NONE;
	}

  if (chStart == chEnd) 
    {
      printf("Shortest path is 0 in cost. Just stay where you are.\n");
    }
  else
    {
      rgnNodes[chStart].iDist = 0;
      rgnNodes[chStart].iPrev = NONE;
      
      enqueue (chStart, 0, NONE);
      
     while (qcount() > 0)
	{
	//   printf("qcount = %d\n", qcount());
	  dequeue (&iNode, &iDist, &iPrev);
	  for (i = 0; i < NUM_NODES; i++)
	    {
	      if ((iCost = AdjMatrix[iNode][i]) != NONE)
		{
		  if ((NONE == rgnNodes[i].iDist) || 
		      (rgnNodes[i].iDist > (iCost + iDist)))
		    {
		      rgnNodes[i].iDist = iDist + iCost;
		      rgnNodes[i].iPrev = iNode;
		      enqueue (i, iDist + iCost, iNode);
		    }
		}
	    }
	}
      
      printf("Shortest path is %d in cost. ", rgnNodes[chEnd].iDist);
      printf("Path is: ");
      print_path(rgnNodes, chEnd);
      printf("\n");
    }
}

int main(int argc, char *argv[]) {
  int i,j,k;
  FILE *fp;

#if defined(SLL)
	ddt = cdsl_sll_init();
#elif defined(DLL)
	ddt = cdsl_dll_init();
#elif defined(DYN)
	ddt = cdsl_dyn_array_init();
#endif

  if (argc<2) {
    fprintf(stderr, "Usage: dijkstra <filename>\n");
    fprintf(stderr, "Only supports matrix size is #define'd.\n");
  }

  /* open the adjacency matrix file */
  fp = fopen (argv[1],"r");

  /* make a fully connected matrix */
  for (i=0;i<NUM_NODES;i++) {
    for (j=0;j<NUM_NODES;j++) {
      /* make it more sparce */
      fscanf(fp,"%d",&k);
	AdjMatrix[i][j]= k;
    }
  }

  /* finds 10 shortest paths between nodes */
  for (i=0,j=NUM_NODES/2;i<20;i++,j++) {
			j=j%NUM_NODES;
      dijkstra(i,j);
  }
  exit(0);
  

}
