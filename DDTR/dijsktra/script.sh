#!/bin/bash

if [ "$1" == "build" ]
then
    mkdir -p ./bin
    
    #SLL
    gcc dijkstraDDTR.c -o ./bin/dijkstraDDTRSLL -pthread -lcdsl \
    -L./../synch_implementations \
    -I./../synch_implementations \
    -DSLL

    #DLL
    gcc dijkstraDDTR.c -o ./bin/dijkstraDDTRDLL -pthread -lcdsl \
    -L./../synch_implementations \
    -I./../synch_implementations \
    -DDLL

    #DYN
    gcc dijkstraDDTR.c -o ./bin/dijkstraDDTRDYN -pthread -lcdsl \
    -L./../synch_implementations \
    -I./../synch_implementations \
    -DDYN

elif [ "$1" == "run" ]
then
    if [ "$2" == "mem_access" ]
    then
        mkdir -p logs/mem_access
        CONFIGURATIONS="SLL DLL DYN"
        for i in $CONFIGURATIONS
        do
            # add task on background
            valgrind --log-file="./logs/mem_access/log_$i.txt" \
            --tool=lackey --trace-mem=yes ./bin/dijkstraDDTR$i input.dat &
        done
        wait
        echo "Finished files located at logs/mem_access"
    elif [ "$2" == "mem_footprint" ]
    then
        mkdir -p logs/mem_footprint
        CONFIGURATIONS="SLL DLL DYN"
        for i in $CONFIGURATIONS;
        do
            valgrind --tool=massif ./bin/dijkstraDDTR$i input.dat &&
            ms_print massif.out.* > ./logs/mem_footprint/log_$i.txt &&
            rm massif.out.*
        done
        echo "Finished files located at logs/mem_footprint"
    else 
        echo "Unsupported option please choose <mem_access, mem_footprint>"
    fi
elif [ "$1" == "count" ]
then
    if [ "$2" == "mem_access" ]
    then
        echo "mem_access"
        for i in $(ls logs/mem_access);
        do
            awk -v var="$i" '/I| L/{count++} END{print var " " count}' logs/mem_access/$i
        done
    elif [ "$2" == "mem_footprint" ]
    then
        echo "mem_footprint"
        for i in $(ls logs/mem_footprint);
        do
            #echo "$i"
            awk -v var="$i" '/KB|MB/{lol=$0;getline; print var " " $1 " " lol}' logs/mem_footprint/$i
        done
    else 
        echo "Unsupported option please choose <mem_access, mem_footprint>"
    fi
else
    echo "Unsupported option please choose <build, run, count>"
fi