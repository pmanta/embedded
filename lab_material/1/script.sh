#!/bin/bash

gcd() (
    if (( $1 % $2 == 0)); then
        echo $2
     else
        gcd $2 $(( $1 % $2 ))
    fi
)

factors() (
    VAR=""
    n=$1
    for i in $(seq 1 $n)
    do
        if [ $(( n / i * i )) = $n ]
        then 
            VAR="$VAR $i"
        fi
    done
    echo $VAR
)

### Q1 ###
if [ "$1" == "-2" ]
then
    gcc -O0 phods2.c -o phods2
    echo "Running 10 times phods2"
    for i in {1..10}
    do
        ./phods2
    done
    rm phods2
elif [ "$1" == "-3" ]
then
    gcc -O0 phods3.c -o phods3
    echo "Running 10 times phods3"
    for i in {1..10}
    do
        ./phods3
    done
    rm phods3
elif [ "$1" == "-4" ]
then
    # find the common factors of the 2 numbers
    N=$(grep "#define N" phods3.c | sed 's/[^0-9]*//g')
    M=$(grep "#define M" phods3.c | sed 's/[^0-9]*//g')
    echo "Finding gcd N = $N, M = $M"
    let GCD=`expr $(gcd $N $M)`
    echo "GCD = $GCD"
    for ((i=1; i<=GCD; i++));
    do  
        let mod=$GCD%$i
        if [ "$mod" -eq 0 ]
        then
            echo "Running for $i"
            sed -i "/#define B/c\#define B $i" phods3.c
            gcc -O0 phods3.c -o phods3
            for j in {1..10}
            do
                ./phods3
            done
            rm phods3
        fi
    done
elif [ "$1" == "-5" ]
then
    # find the factors of N and M
    N=$(grep "#define N" phods3.c | sed 's/[^0-9]*//g')
    M=$(grep "#define M" phods3.c | sed 's/[^0-9]*//g')
    echo "Finding factors of N = $N, M = $M"
    FN=$(factors $N)
    FM=$(factors $M)
    let min=2147483647 # max int
    for i in $FN;
    do
        for j in $FM;
        do
            # Run 10 times and compute the avg
            # compute the minimum time and minimum combination in parallel
            sed -i "/#define Bx/c\#define Bx $i" phods5.c
            sed -i "/#define By/c\#define By $j" phods5.c
            gcc -O0 phods5.c -o phods5
            let sum=0
            for k in {1..10}
            do
                let res=$(./phods5)
                sum=$(( sum + res ))
            done
            let avg=$((sum / 10))
            echo "$avg $i $j"
            if [ $avg -lt $min ]
            then
                echo "min changed to $avg"
                let min=avg
                let Bx=$i
                let By=$j
            fi
            rm ./phods5
        done
    done
    echo "Min = $min for Bx = $Bx , By = $By"
elif [ "$1" == "-5h" ]
then
    # find the factors of N and M
    N=$(grep "#define N" phods3.c | sed 's/[^0-9]*//g')
    M=$(grep "#define M" phods3.c | sed 's/[^0-9]*//g')
    echo "Finding factors of N = $N, M = $M"
    FN=$(factors $N)
    FM=$(factors $M)
    let min=2147483647 # max int
    arr=($FN) #convert from string to array
    i=${arr[-1]}
    for j in $FM;
    do
        # Run 10 times and compute the avg
        # compute the minimum time and minimum combination in parallel
        sed -i "/#define Bx/c\#define Bx $i" phods5.c
        sed -i "/#define By/c\#define By $j" phods5.c
        gcc -O0 phods5.c -o phods5
        let sum=0
        for k in {1..10}
        do
            let res=$(./phods5)
            sum=$(( sum + res ))
        done
        let avg=$((sum / 10))
        echo "$avg $i $j"
        if [ $avg -lt $min ]
        then
            echo "min changed to $avg"
            let min=avg
            let By=$j
        fi
        rm ./phods5
    done

    j=$By
    let min=2147483647 # max int
    for i in $FN;
    do
        # Run 10 times and compute the avg
        # compute the minimum time and minimum combination in parallel
        sed -i "/#define Bx/c\#define Bx $i" phods5.c
        sed -i "/#define By/c\#define By $j" phods5.c
        gcc -O0 phods5.c -o phods5
        let sum=0
        for k in {1..10}
        do
            let res=$(./phods5)
            sum=$(( sum + res ))
        done
        let avg=$((sum / 10))
        echo "$avg $i $j"
        if [ $avg -lt $min ]
        then
            echo "min changed to $avg"
            let min=avg
            let Bx=$i
        fi
        rm ./phods5
    done
    echo "Min = $min for Bx = $Bx , By = $By"
fi