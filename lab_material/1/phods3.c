/*Parallel Hierarchical One-Dimensional Search for motion estimation*/
/*Initial algorithm - Used for simulation and profiling             */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define N 144 /*Frame dimension for QCIF format*/
#define M 176 /*Frame dimension for QCIF format*/
#define B 16
#define p 7   /*Search space. Restricted in a [-p,p] region around the \
                original location of the block.*/

void read_sequence(int current[N][M], int previous[N][M])
{
    FILE *picture0, *picture1;
    int i, j;

    if ((picture0 = fopen("akiyo0.y", "rb")) == NULL)
    {
        printf("Previous frame doesn't exist.\n");
        exit(-1);
    }

    if ((picture1 = fopen("akiyo1.y", "rb")) == NULL)
    {
        printf("Current frame doesn't exist.\n");
        exit(-1);
    }

    /*Input for the previous frame*/
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < M; j++)
        {
            previous[i][j] = fgetc(picture0);
        }
    }

    /*Input for the current frame*/
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < M; j++)
        {
            current[i][j] = fgetc(picture1);
        }
    }

    fclose(picture0);
    fclose(picture1);
}

void phods_motion_estimation(int current[N][M], int previous[N][M],
                             int vectors_x[N/B][M/B], int vectors_y[N/B][M/B])
{
    int x, y, i, j, k, l, p1, p2, q2, distx, disty, S, min1, min2, bestx, besty;
    int tx[4], ty[4];
    distx = 0;
    disty = 0;

    /*For all blocks in the current frame*/
    for (x = N/B; --x;) // Loop reversal
    {
        tx[0] = B * x;
        for (y = M/B; --y;)
        {
            ty[0] = B*y;
            S = 4;
            vectors_x[x][y] = 0;
            vectors_y[x][y] = 0;

            while (S > 0)
            {
                min1 = 255 * B * B;
                min2 = 255 * B * B;

                /*For all candidate blocks in X dimension*/
                for (i = -S; i < S + 1; i += S)
                {
                    distx = 0;
                    disty = 0; // loop merging

                    /*For all pixels in the block*/
                    for (k = B; --k;) // change instruction
                    {
                        tx[1] = tx[0] + k;               // B*x + k
                        tx[2] = tx[1] + vectors_x[x][y]; // B*x + vectors_x[x][y] + k
                        tx[3] = tx[2] + i;               // B*x + vectors_x[x][y] + i + k
                        for (l = B; --l;)
                        {
                            ty[1] = ty[0] + l;               // B*y + l
                            ty[2] = ty[1] + vectors_y[x][y]; // B*y + vectors_y[x][y] + l
                            ty[3] = ty[2] + vectors_y[x][y]; // B*y + vectors_y[x][y] + i + l
                            p1 = current[tx[1]][ty[1]];
                            p2 = previous[tx[3]][ty[2]];
                            q2 = previous[tx[2]][ty[3]];

                            if ((tx[3]) < 0 ||
                                (tx[3]) > (N - 1) ||
                                (ty[2]) < 0 ||
                                (ty[2]) > (M - 1))
                            {
                                p2 = 0;
                            }

                            /* Loop merging with below */
                            if ((tx[2]) < 0 ||
                                (tx[2]) > (N - 1) ||
                                (ty[3]) < 0 ||
                                (ty[3]) > (M - 1))
                            {
                                q2 = 0;
                            }

                            disty += abs(p1 - q2);
                            distx += abs(p1 - p2);
                        }
                    }

                    if (distx < min1)
                    {
                        min1 = distx;
                        bestx = i;
                    }
                    /* loop merging */
                    if (disty < min2)
                    {
                        min2 = disty;
                        besty = i;
                    }
                }

                S = S / 2;
                vectors_x[x][y] += bestx;
                vectors_y[x][y] += besty;
            }
        }
    }
}

int main()
{
    int current[N][M], previous[N][M], motion_vectors_x[N/B][M/B],
        motion_vectors_y[N/B][M/B], i, j;
    struct timeval tv;
    read_sequence(current, previous);
    gettimeofday(&tv, NULL);
    long long int ts1, ts2;
    ts1 = tv.tv_sec * 1000000 + tv.tv_usec; // microseconds
    phods_motion_estimation(current, previous, motion_vectors_x, motion_vectors_y);
    gettimeofday(&tv, NULL);
    ts2 = tv.tv_sec * 1000000 + tv.tv_usec;
    printf("%lld\n", ts2 - ts1);
    return 0;
}
