#!/bin/bash

### Q1 ###
if [ "$1" == "-1" ]
then
    gcc -O0 tables_custom.c -o tables_custom
    echo "Running 10 times without optimization"
    for i in {1..10}
    do
        ./tables_custom
    done
    rm tables_custom
elif [ "$1" == "-2" ]
then
    # Exchaustive
    echo "Running with the result of exhaustive"
    gcc -O0 tables_custom.c -o tables_custom -D EXHA
    for i in {1..10}
    do
        ./tables_custom
    done
    # Randomsearch
    echo "Running with the result of random search"
    gcc -O0 tables_custom.c -o tables_custom -D RAND
    for i in {1..10}
    do
        ./tables_custom
    done
    # Simplex
    echo "Running with the result of simplex"
    gcc -O0 tables_custom.c -o tables_custom -D SIMP
    for i in {1..10}
    do
        ./tables_custom
    done
fi