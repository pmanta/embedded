#include <stdio.h>
#include <assert.h>
#include <string.h>
extern size_t mstrlen(const char *s);
extern int mstrcmp(const char *s1, const char *s2);
extern char *mstrcat(char *dest, char *src);
extern char *mstrcpy(char *dest, const char *src);

int main() {
	char buf[1024];
	printf("%d\n", mstrlen("hello there"));
	assert(mstrlen("hello there") == strlen("hello there"));
	assert(mstrlen("") == strlen(""));
	int i = strlen("12");
	printf("%d\n", i);
	printf("%d %d %d\n", mstrcmp("hel","hel"), mstrcmp("b", "adfa"), mstrcmp("aear", "aearf")); 
	assert(mstrcmp("hel","hel") == strcmp("hel","hel"));
	printf("%s\n", mstrcat(mstrcpy(buf, "hello "), "there"));
	printf("%s\n", mstrcat(mstrcpy(buf, "F2Qd#~5p;|9J!-h\\Dul\\A7RIW5UUC#|MDCn0E9`e44unt1MWDK|dNyq9L?AB(}"), "Zaq|XCSRHD&8it3[%4`F!On6z4-~W0dd[)[VeMetJH?HpXgO?ss"));
	return 0;
}
