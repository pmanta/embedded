#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

#define G_DATA_SIZE 38

/*
 * termios setup of the serial port
 */
void termios_setup(int fd)
{
    struct termios config;
    
    if(tcgetattr(fd, &config) < 0) {
        perror("Error tcgetattr");
        exit(EXIT_FAILURE);
    }
    
    config.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
                                INLCR | PARMRK | INPCK | ISTRIP | IXON);

    config.c_oflag = 0;
    config.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
    config.c_cflag &= ~(CSIZE | PARENB);
    config.c_cflag |= CS8;
    config.c_cc[VMIN]  = 1;
    config.c_cc[VTIME] = 0;
    if(cfsetispeed(&config, B9600) < 0 || cfsetospeed(&config, B9600) < 0) {
        perror("Error setting up speed for serial port");
        exit(EXIT_FAILURE);
    }

    if(tcsetattr(fd, TCSAFLUSH, &config) < 0) {
        perror("Error applying configuration");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char **argv)
{
    char buf[64] = {'\0'};
    char trash[64] = {'\0'};
    ssize_t rn = 0, wn = 0, tmp = 0;
    int fd1;

    /* 
     * Read from stdin 
     * then open "/dev/pts/3" and send it to the assembly
     * then the assembly will manipulate it 
     */
    
    /* 
     * Open the device because otherwise
     * we cannot see the data from the guest
     */
    fd1 = open(argv[1], O_RDWR | O_NOCTTY);
    termios_setup(fd1);     
    while(1) {
        wn = rn = tmp = 0;
        printf("Please give a string to send to guest:\n");
        rn = read(STDIN_FILENO, buf, 64);
        if (rn < 0) {
            perror("Error reading stdin");
            exit(EXIT_FAILURE);
        }
    
        /* add null terminating char at position 63 */
        buf[63] = '\0';
   
        if (fd1 < 0) {
            fprintf(stderr, "Cannot open file %s", argv[1]);
            perror("");
            exit(EXIT_FAILURE);
        };
   
        /* Persist write the data to the guest */
        wn = write(fd1, buf + tmp, rn);
        while (rn > 0 && (wn != rn)) {
            if (wn < 0 && errno == EINTR)
                continue;
            if (wn < 0) {
                fprintf(stderr, "Error on writting to %s", argv[1]);
                perror("");
                exit(EXIT_FAILURE);
            }
            rn -= wn;
            tmp += wn;
            wn = write(fd1, buf + tmp, rn);
        }
    
        /* Now wait for the result from the guest system */
        usleep ((7 + 25) * 100);
    
        /* Clear written data */
        ssize_t l = wn + tmp; // this is how much data we wrote
        ssize_t p = 0;
        p = read(fd1, trash, l+1);
        while (p != l+1) {
            rn = read(fd1, trash, l+1-p);
            p += rn;
        }
        printf("Cleared data in serial\n");
    
        char guest_data[G_DATA_SIZE];
        tmp = 0;
    
        /* Read until you read 64+1 bytes cause of '\r' */
        rn = read(fd1, guest_data + tmp, G_DATA_SIZE-tmp);
        while (G_DATA_SIZE-tmp > 0 && (rn != G_DATA_SIZE-tmp)) {
            tmp += rn;
            rn = read(fd1, guest_data + tmp, G_DATA_SIZE-tmp);
        }

        if (rn < 0) {
            perror("Error reading from serial");
            exit(EXIT_FAILURE);
        }

        fprintf(stdout, "Response: %s\n", guest_data);
    }
    close(fd1);
    return 0;
}
