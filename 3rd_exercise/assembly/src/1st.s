.text
.global main

clear_buffer:
	push {r0, r1, r2, lr}
	mov r1, #0
	mov r2, #0
clear:
	cmp r1, #31
	strltb r2, [r0, r1]
	add r1, r1, #1
	bne clear
	
	pop {r0, r1, r2, lr}
	bx lr

main:
	mov r0, #1 		/* first argument -> stdout*/
	ldr r1, =output_string 	/*second argument -> memory where output string is stored*/
	mov r2, #len 		/* third argument -> number of bytes of output message. len is a constant and thus its valus is moved to r2 */
			
	mov r7, #4 		/* number of write system call */
	swi 0	

inp1:
	mov r0, #0 		/* first argument -> stdin*/
	ldr r1, =inp_str 	/*second argument -> memory where input string will be stored*/
	mov r2, #32 		/* third argument -> number of bytes to be read */
	mov r7, #3 		/* number of read system call */
	swi 0
	ldr r0, =inp_str 	/* address of the data */
	ldrb r3, [r0]
	ldrb r4, [r0, #1]
	mov r5, #0		/* check if user input is */
	cmp r3, #'Q'		/* 'Q\n' */
	moveq r5, #1		/* if it quit the program */
	cmp r4, #'\n'
	movne r5, #0
	cmp r5, #1
	beq exit
	bl  enc			/* call encrypt */
	mov r0, #1 		/* first argument -> stdout*/
	ldr r1, =inp_str 	/* second argument -> memory where output string is stored */
	mov r2, #32 		/* third argument -> number of bytes of output message */
	mov r7, #4  		/* number of write system call */
	swi 0
	ldr r0, =inp_str
	bl clear_buffer
	b main
	
exit:	mov r0, #0 		/* first argument -> status = 0, everything ok*/
	mov r7, #1 		/* number of exit system call */
	swi 0

enc:
	push {r0,r1,r2,r3,lr}	/* save registers */
	mov r1, #0		/* loop counter */
	
loop:
	cmp r1, #31		/* if we pass 31 return */
	bgt return	
	ldrb r2, [r0, r1]	/* load byte in r0+r1 */
	cmp r2, #'0'		/* check if char is */
	blt continue		/* between 0-9 */
	sub r3, r2, #'0'	/* by comparing char - '0' */
	cmp r3, #'9'-'0'	/* with '9' - '0' */
	bgt check_upper		// if the difference is bigger then its after '9'
	add r2, r2, #5		// else 
	cmp r2, #'9'		// if we exceed 9 sub 10
	subgt r2, #10
	strb r2, [r0, r1]	// store the byte
	b continue

check_upper:
	cmp r2, #'A'		// cmp with 'A'
	blt continue		// if we are between 'A' and '9' don't do nothing
	sub r3, r2, #'A'	// if we are greater or equal
	cmp r3, #'Z'-'A'	// check if we are between 'A'-'Z'
	addle r2, r2, #32	// if we are, add 32 to go to 'a'-'z'
	strleb r2, [r0, r1]	// store
	ble continue		// and continue to next char
	
check_lower:
	cmp r2, #'a'		// cmp with 'a'
	blt continue		// if we are between 'a' and 'Z' don't do nothing
	sub r3, r2, #'a'	// if we are greater
	cmp r3, #'z'-'a'	// check if we are in 'a'-'z'
	suble r2, r2, #32	// if so sub 32 to go to 'A'-'Z'
	strleb r2, [r0, r1]	// store and continue

continue:
	add r1, r1, #1
	b loop
return:
	pop {r0,r1,r2,r3,lr}
        bx lr
	
.data
	output_string: .ascii "Input a string of up to 32 chars long: " /* location of output string in memory */
len = . - output_string /* length of output_string is the current memory indicated by (.) minus the memory location of the first element of string. Len does not occupy memory. It is a constant for the assembler. */
	inp_str: .ascii "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0" /* pre-allocate 8 bytes for input string, initialize them with null character '/0'*/
