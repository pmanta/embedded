.text
.global main

// Clear the buffer starting from r1
clear_buffer:
	push {r1, r9, r10, r11, lr} 	//save all register in stack
	mov r9, #0			//our loop counter
	mov r10, #0x00			//the fill character
loop:
	ldrb r11, [r1, r9]		//load the byte at r1+r9
	cmp r11, r9			//if is \0 exit
	beq ret		
	cmp r9, #63			//if our counter is 63 exit again
	beq ret
	strb r10, [r1, r9]		//store at the address r1+r9 the \0 char
	add r9, r9, #1			//increment counter i
	b loop	
ret:
	pop {r1, r9, r10, r11, lr}	//pop saved register and the return address
	bx lr				//brach to return address

// counts the times we see a char in inp_str
count_chars:
	push {r0, r1, r2, r3, r4, lr}
	ldr r0, =inp_str
	mov r1, #0		//counter
	ldr r2, =ascii_array
bar:	
	ldrb r3, [r0, r1] 	//load byte at r0+r1 address
	cmp r3, #0
	beq end
	cmp r3, #32		//check if this is a space
	ldrneb r4, [r2, r3]	//load the current counter of the char
	addne r4, r4, #1	//increase it
	strneb r4, [r2, r3]	//update memory
	add r1, r1, #1		//go to the next char at inp_str
	cmp r1, #64		//if we pass the 63 dont loop again 
	bne bar
end:	
	pop {r0, r1, r2, r3, r4, lr}
	bx lr

// returns count at r3 and char at r4
find_max:
	push {r0, r1, lr}
	mov r0, #1			//counter
	ldr r1, =ascii_array
	ldrb r3, [r1]			//max = aa[0]
	mov r4, #0
ho:
	ldrb r2, [r1, r0]		//load the counter of the char
	cmp r2, r3			//compare with current max
	movgt r3, r2			//update max if needed
	movgt r4, r0			//update max count
	add r0, r0, #1			//increase counter
	cmp r0, #256			//while i !=256
	bne ho
	pop {r0, r1, lr}		//pop used registers
	bx lr


clear_counters:
	push {r0, r1, r2, r3, lr}
	mov r0, #0 			// our counter
	ldr r1, =ascii_array		// start of counter array
	mov r2, #0			// zeroing
while:
	cmp r0, #256
	strneb r2, [r1, r0]
	addne r0, r0, #1
	bne while
	pop {r0, r1, r2, r3, lr}
	bx lr
	
main:
	ldr r0, =device_name 	/* first arg pathname */
	ldr r1, =flags 		/* O_RDWR | O_NOCTTY | O_SYNC load from memory because mov has limit of bits */
	ldr r1, [r1]
	mov r2, #0x40 		/* third argument -> default mode_t */	
	mov r7, #5 		/* number of open system call */
	swi 0	 	
	mov r10, r0 		// save fd returned
read:	
	mov r0, r10
	ldr r1, =inp_str 	/* second argument -> memory where input string will be stored */
	mov r2, #64 		/* third argument -> number of bytes to be read */
	mov r7, #3 		/* number of read system call */
	swi 0			// call read syscall	

sleep:	
	mov r0, #3200		// call sleep to let the c program clear the data
	bl usleep

write:	
	bl count_chars
	bl find_max
	ldr r11, =out_str
	strb r4, [r11, #27]	//add the char to the out_str
	add r3, r3, #'0'
	strb r3, [r11, #30]	//add the count to the out_str
	mov r0, r10		// restore the fd
	mov r1, r11		// restore the message pointer
	mov r2, #out_len 		/* third argument -> number of bytes of output message */
	mov r7, #4 		/* number of write system call */
	swi 0			// call syscall write to serial
	ldr r1, =inp_str
	bl clear_buffer
	bl clear_counters
	b read		
exit:
	mov r0, #0 		/* first argument -> status = 0, everything ok*/
	mov r7, #1 		/* number of exit system call */
	swi 0
	
.data
	device_name: .asciz "/dev/ttyAMA0" /* the tty serial device name used to get file descriptor */
	inp_str: .asciz "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0" /* pre-allocate 64 bytes for input string, initialize them with null character '/0'*/
	flags: .word 0x102	// flags for opening serial port
	out_str: .asciz "The most frequent char is '\0' \0 times"	// this will hold the response we sent to the host
	out_len = . - out_str
	ascii_array: .space 255 // counters for all ascii characters
