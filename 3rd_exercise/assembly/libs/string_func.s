.text
.global mstrcpy
.global mstrlen
.global mstrcmp
.global mstrcat

.type mstrcpy, %function
.type mstrlen, %function
.type mstrcat, %function
.type mstrcmp, %function

mstrcpy:
	.fnstart		//unwind table to allow exceptions
	push {r5, r6, lr}
	mov r5, #0		// our counter
cpy:
	ldrb r6, [r1, r5]	// load byte from src
	cmp r6, #0		// check if its the null
	strneb r6, [r0, r5]	// if its not store it to dest
	addne r5, r5, #1	// got to next byte
	bne cpy			// do it again
	strb r6, [r0, r5]	// else store the null char in dest also
	pop {r5, r6, lr}	// and return to caller
	bx lr
	.fnend


mstrcmp:
	.fnstart
	push {r5, r6, r7, r8, lr}
	mov r5, #0
	mov r8, #0		// default equal
cmpr:	
	ldrb r6, [r0, r5]	// load s1 char
	ldrb r7, [r1, r5]	// load s2 char
	cmp r6, r7		// compare them
	movgt r8, #1		// if not equal r8=0
	movlt r8, #0-1
	bne ret			// return
	addeq r5, r5, #1	// else go to next char
	cmpeq r6, #0x00		// if they are equal check if string ended
	bne cmpr

ret:
	mov r0, r8		// return value at r0
	pop {r5, r6, r7, r8, lr}
	bx lr
	.fnend

mstrcat:
	.fnstart
	push {r5, r6, r7, lr}
	mov r5, r0		// save the pointer of the dest
	bl mstrlen		// find the length of the dest
	add r0, r5, r0 		// make r0 point to end of dest
	mov r7, #0		// our counter
cat:
	ldrb r6, [r1, r7]	// load byte of src
	cmp r6, #0		// if not end of string (null)
	strneb r6, [r0, r7]	// store byte at the r0 address
	addne r7, r7, #1	// increment i
	bne cat			// again
	strb r6, [r0, r7]	// add the \0 char 
	mov r0, r5		// else return
	pop {r5, r6, r7, lr}	// pointer to dest
	bx lr
	.fnend

mstrlen:
	.fnstart
	push {r5, r6, lr}
	mov r5, #0		// counter
count:
	ldrb r6, [r0, r5]	// load current char
	cmp r6, #0x00		// if its not '\0'
	addne r5, r5, #1	// increase  counter
	bne count		// and go again
	mov r0, r5		// return value at r0
	pop {r5, r6, lr}	// return to caller
	bx lr
	.fnend
